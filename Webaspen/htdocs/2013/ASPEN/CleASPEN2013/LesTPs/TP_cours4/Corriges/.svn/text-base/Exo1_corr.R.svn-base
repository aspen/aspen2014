library(DiceKriging)

N <- 200 #nb de points de la trajectoire
x <- seq(0,1,length.out=N)
dist_all <- as.matrix(dist(x))
n <- 10 # simulation de n trajectoires
Z1 <- matrix(rnorm(N*n),N,n)  # simulation vecteur gaussien centr� indep
f0 = rep(0,N)

# covariance gaussienne
sig2 <- 0.5
theta <- 0.05
## theta <- 0.2
lambda <- 10^(-10)
cov = sig2*(exp(-(dist_all/theta)^2) + lambda * diag(1,N))  # cov between x
Z2 <- matrix(0,N,n)
for (i in 1:n){
	Z2[,i] <- f0+t(chol(cov))%*%Z1[,i]
}
plot(x,Z2[,1], main=paste('Simulation trajectoires PG de covariance gaussienne avec theta =',theta),type='l',ylim=c(min(Z2),max(Z2)))
for (i in 2:n)
	lines(x,Z2[,i],col=i)


# covariance exponentielle
sig2 <- 0.5
theta <- 0.05
cov <- sig2*exp(-(dist_all/theta))
Z2 <- Z1
for (i in 1:n)
  Z2[,i] <- f0+t(chol(cov))%*%Z1[,i]
plot(x,Z2[,1], main=paste('Simulation trajectoires PG de covariance exponentielle avec theta =',theta),type='l',ylim=c(min(Z2),max(Z2)))
for (i in 2:n)
	lines(x,Z2[,i],col=i)



# covariance gaussienne avec effet de p�pite
sig2 <- 0.5
theta <- 0.2
lambda <- 0.05
cov <- sig2*(exp(-(dist_all/theta)^2) + lambda * diag(1,N))
Z2 <- Z1
for (i in 1:n)
  Z2[,i] <- f0+t(chol(cov))%*%Z1[,i]
plot(x,Z2[,1], main=paste('Simulation trajectoires PG de covariance gaussienne avec theta =',theta),type='l',ylim=c(min(Z2),max(Z2)))
for (i in 2:n)
  lines(x,Z2[,i],col=i)

##### ou avec DiceKriging
type <- "exp"
coef <- c(theta = 0.05)
sigma <- 0.5
model <- km(design=x, response=rep(0,N),coef.trend = 0,covtype=type, coef.cov=coef, coef.var=sigma)
Z2 <- simulate(model, nsim=10, newdata=NULL)
plot(x,Z2[1,], main=paste('Simulation trajectoires PG de covariance exponentielle avec theta =',theta),type='l',ylim=c(min(Z2),max(Z2)))
for (i in 2:n)
  lines(x,Z2[i,], main=paste('Simulation trajectoires PG de covariance expo avec \theta =',theta))


type <- "gauss"
lambda <- 10^(-10) # terme diagonal pour am�liorer le conditionnement de la matrice de covariance
model <- km(design=x, response=rep(0,N),coef.trend = 0,covtype=type, coef.cov=coef, coef.var=sigma,nugget=lambda)
Z2 <- simulate(model, nsim=10, newdata=NULL)
plot(x,Z2[1,], main=paste('Simulation trajectoires PG de covariance exponentielle avec theta =',theta),type='l',ylim=c(min(Z2),max(Z2)))
for (i in 2:n)
  lines(x,Z2[i,], main=paste('Simulation trajectoires PG de covariance expo avec \theta =',theta))


###############################################################
## Exercice 1.b : simulations processus gaussien 2D          ##
###############################################################

rm(list=ls(all=TRUE))

# Initialisation des param�tres
d = 2 # dimension des entr�es
N_dim = 50 # nb de points dans chaque dimension de l'espace
N_tot = N_dim^d 
x <- as.matrix(expand.grid(seq(0,1,length.out=N_dim),seq(0,1,length.out=N_dim)))
mu <- 0
sig2 <- 0.5
dist1 <- as.matrix(dist( x[,1] ))
dist2 <- as.matrix(dist( x[,2] ))

# covariance gaussienne isotrope
theta = 0.1
lambda <- 10^(-10)  # terme diagonal pour am�liorer le conditionnement de la matrice de covariance
cov = sig2*(exp(-((dist1/theta)^2 +(dist2/theta)^2))+ lambda * diag(1,N_tot))
f0 = x %*% c(0,0)

n <- 1 # simulation de n trajectoires
Z1 <- matrix(rnorm(N_tot*n),N_tot,n) # simulation vecteur gaussien centr� indep
Z2=f0+t(chol(cov))%*%Z1  # puis transformation lin�aire

par(mfrow=c(1,1))
filled.contour(matrix(Z2,N_dim,N_dim),main=paste('Simulation trajectoires PG de covariance gaussienne isotrope avec theta =', theta),color.palette=heat.colors)

# covariance gaussienne anisotrope
theta1 = 0.1
theta2 = 0.03
cov = sig2*(exp(-((dist1/theta1)^2 +(dist2/theta2)^2)) + lambda * diag(1,N_tot))
f0 = x %*% c(0,0)

Z2=f0+t(chol(cov))%*%Z1  # puis transformation lin�aire
filled.contour(matrix(Z2,N_dim,N_dim),main=paste('Simulation trajectoires PG de covariance gaussienne anisotrope avec theta1 =', theta1,'& theta2=',theta2),color.palette=heat.colors)

# covariance exponentielle anisotrope
cov = sig2*exp(-(dist1/theta1 + dist2/theta2)) 
f0 = x %*% c(0,0)

Z2=f0+t(chol(cov))%*%Z1  # puis transformation lin�aire
filled.contour(matrix(Z2,N_dim,N_dim),main=paste('Simulation trajectoires PG de covariance exponentielle anisotrope avec theta1 =', theta1,'& theta2=',theta2),color.palette=heat.colors)














