\name{morris.mexico}
\alias{morris.mexico}
\title{Methode de Morris a la sauce mexicaine}
\usage{morris.mexico(model, factors, r, design, binf=0, bsup=1, scale=TRUE, ...)
}
\description{Adaptation de la méthode de Morris pour l'EC Mexico}
\value{voir la méthode morris de la librairie sensitivity}
\note{Corrige un problème détecté dans la version 1.0 de sensitivity en
normalisant les facteurs d'entrée avant les calculs principaux}
\arguments{\item{model}{voir la méthode morris de la librairie sensitivity}
\item{factors}{voir la méthode morris de la librairie sensitivity}
\item{r}{voir la méthode morris de la librairie sensitivity}
\item{design}{voir la méthode morris de la librairie sensitivity}
\item{binf}{voir la méthode morris de la librairie sensitivity}
\item{bsup}{voir la méthode morris de la librairie sensitivity}
\item{scale}{voir la méthode morris de la librairie sensitivity}
\item{...}{voir la méthode morris de la librairie sensitivity}
}

