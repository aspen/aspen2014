\name{wwdm.climates}
\alias{wwdm.climates}
\alias{wwdm.climates}
\title{Series climatiques sur 14 annees, utilisees par le modele wwdm}
\description{Séries climatiques sur 14 années, utilisées par le modèle wwdm}
\docType{data}
\value{data.frame à N lignes (1 par jour) et 4 colonnes (ANNEE, RG, Tmin, Tmax)}

