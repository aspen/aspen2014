compareMC <- function (indTaille,p=NULL,a=NULL) 
{
# Comparaison des méthodes en fonctions du nombre d'evaluation
#
if(is.null(p) & (is.null(a)))
  stop("Le nombre de parametres est inconnu")

if(is.null(p)) 
  {
  p <-length(a)
  sobol_theor <-sobol.gfunc(a)
}

if(is.null(a))
{
  a<- c(0,0,0,1,1,99,99,99,99)
  cat("a est fixe a (0,0,0,1,1,99,99,99,99)")
}

if(is.null(indTaille)) indTaille <-c(1000,400,1000,4000,8000)
# indTaille <- seq(1000,16000,500)
q <- length(indTaille)

# Nombre d'evaluation du modele pour chaque methode et chaque taille de plan
nb_runs <- array(0,c(q,2))
# Indice de Sobol d ordre 1 pour chaque methode et chaque taille de plan
indSobol_1 <- array(0,c(q,p,2))
indSobol_1_bias <- array(0,c(q,p,2))
indSobol_1_std <- array(0,c(q,p,2))
# Indice de Sobol totaux pour chaque methode et chaque taille de plan
indSobol_T <- array(0,c(q,p,2))
indSobol_T_bias <- array(0,c(q,p,2))
indSobol_T_std <- array(0,c(q,p,2))

for (i in 1:q) {
   
  # Echantillon
  n <- indTaille[i]
  
  X1 <- data.frame(matrix(runif(p*n), nrow = n))
  X2 <- data.frame(matrix(runif(p*n), nrow = n))  

#   # Sobol2007
  print(i)
  xsobol2007 <- sobol2007(model = g.func, X1 = X1, X2 = X2, nboot = 100,a=a)
  nb_runs[i,1] <- (2+p)*indTaille[i]
  indSobol_1[i,,1]<-xsobol2007$S[,1]
  indSobol_1_bias[i,,1] <-xsobol2007$S[,2]
  indSobol_1_std[i,,1] <-xsobol2007$S[,3]
  indSobol_T[i,,1]<-xsobol2007$T[,1]
  indSobol_T_bias[i,,1] <-xsobol2007$T[,2]
  indSobol_T_std[i,,1] <-xsobol2007$T[,3]
#   
#   # SobolEff
  xsobolEff <- sobolEff(model = g.func, X1 = X1, X2 = X2, nboot = 100,a=a)
  nb_runs[i,2] <- (1+p)*indTaille[i]
  indSobol_1[i,,2]<-xsobolEff$S[,1]
  indSobol_1_bias[i,,2] <-xsobolEff$S[,2]
  indSobol_1_std[i,,2] <-xsobolEff$S[,3]
# sobolEff: as d'estimation des indices totaux
 
#  
xx <-list(method1=sobol2007,method2=sobolEff, Q=q,P=p, NB_RUNS=nb_runs,
          sobol_theor=sobol_theor,
        indSobol_1=indSobol_1, indSobol_1_bias=indSobol_1_bias, 
                indSobol_1_std=indSobol_1_std,
                indSobol_T=indSobol_T,indSobol_T_bias=indSobol_T_bias,
                indSobol_T_std=indSobol_T_std,call=match.call())
}

# Visualisation
#   
# Graphe S1 et S2 pour les deux methodes
  x11()
  par(mfrow=c(3,p))
 par_old <- par()

# Indices de sobol
# ----------------
# Premier facteur
ylim1=range(xx$indSobol_1[,1,1],xx$indSobol_1[,1,2])
xlim1=range(xx$NB_RUNS[,1],xx$NB_RUNS[,2])
plot(xx$NB_RUNS[,1],xx$indSobol_1[,1,1],col="blue",ylim=ylim1,xlim=xlim1,ylab="S1",
     xlab="Taille de l'échantillon",type='l')
# superpose
par(new=TRUE,ann=FALSE)
   plot(xx$NB_RUNS[,2],xx$indSobol_1[,1,2],ylim=ylim1,xlim=xlim1,col="red",ylab="S1",
      xlab="Taille de l'échantillon",type='l')

# Deuxieme facteur
ylim1=range(xx$indSobol_1[,2,1],xx$indSobol_1[,2,2])
xlim1=range(xx$NB_RUNS[,1],xx$NB_RUNS[,2])
plot(xx$NB_RUNS[,1],xx$indSobol_1[,2,1],ylim=ylim1,xlim=xlim1,col="blue",ylab="S1",
     xlab="Taille de l'échantillon",type='l')
# superpose
par(new=TRUE,ann=FALSE)
plot(xx$NB_RUNS[,2],xx$indSobol_1[,2,2],ylim=ylim1,xlim=xlim1,col="red",ylab="S1",
      xlab="Taille de l'échantillon",type='l')

# Biais de l'estimation de chaque facteur
# -----------------------------------------
# Premier facteur
ylim1<-range(xx$indSobol_1_bias[,1,1],xx$indSobol_1_bias[,1,2])

plot(xx$NB_RUNS[,1],xx$indSobol_1_bias[,1,1],xlim=xlim1,ylim=ylim1,col="blue",ylab="biais - estimation S1",
     xlab="Taille de l'échantillon",type='l')
# superpose
par(new=TRUE,ann=FALSE)
plot(xx$NB_RUNS[,2],xx$indSobol_1_bias[,1,2],xlim=xlim1,ylim=ylim1,col="red",ylab="biais - estimation S1",
      xlab="Taille de l'échantillon",type='l')

# Deuxieme facteur
ylim1<-range(xx$indSobol_1_bias[,2,1],xx$indSobol_1_bias[,2,2])
plot(xx$NB_RUNS[,1],xx$indSobol_1_bias[,2,1],xlim=xlim1,ylim=ylim1,col="blue",ylab="biais - estimation S2",
     xlab="Taille de l'échantillon",type='l')
# superpose
par(new=TRUE,ann=FALSE)
   plot(xx$NB_RUNS[,2],xx$indSobol_1_bias[,2,2],xlim=xlim1,ylim=ylim1,col="red",ylab="biais - estimation S2",
      xlab="Taille de l'échantillon",type='l')

# # Standard error - estimation des facteurs d'ordre 1
# # ---------------------------------------------------
# # Premier facteur
# ylim1=range(xx$indSobol_1_std[,1,1],xx$indSobol_1_std[,1,2])
# plot(xx$NB_RUNS[,1],xx$indSobol_1_std[,1,1],xlim=xlim1,ylim=ylim1,col="blue",ylab="Standard error S1",
#        xlab="Taille de l'échantillon",type='l')
# # superpose
# par(new=TRUE)
# plot(xx$NB_RUNS[,2],xx$indSobol_1_std[,1,2],xlim=xlim1,ylim=ylim1,col="red",ylab="Standard error S1",
#       xlab="Taille de l'échantillon",type='l')
# 
# # Deuxieme facteur
# ylim1=range(xx$indSobol_1_std[,2,1],xx$indSobol_1_std[,2,2])
# plot(xx$NB_RUNS[,1],xx$indSobol_1_std[,2,1],xlim=xlim1,ylim=ylim1,col="blue",ylab="Standard error S1",
#      xlab="Taille de l'échantillon",type='l')
# # superpose
# par(new=TRUE)
# 
# plot(xx$NB_RUNS[,2],xx$indSobol_1_std[,2,2],xlim=xlim1,ylim=ylim1,col="red",ylab="Standard Error S1",
#      xlab="Taille de l'échantillon",type='l')

return(xx)

}