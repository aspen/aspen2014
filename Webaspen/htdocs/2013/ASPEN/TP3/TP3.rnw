\documentclass[a4paper]{article}
\usepackage{Sweave}

\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{url}
\usepackage{a4wide}
\usepackage{color}

\def\I{\mbox{\bf I}}
\def\R{\mathbb{R}}
\def\Z{\mathbb{Z}} 
\def\C{\mathbb{C}} 
\def\N{\mathbb{N}} 
\def\1{\mathbf{1}}

\setlength{\textwidth}{17cm}
\setlength{\textheight}{26cm}

%% Decalage d'affichage et d'impression 
%\voffset=-1.3cm
%\setlength{\hoffset}{-1.4cm}
%\setlength{\oddsidemargin}{2cm}       %% offset des pages impaires
%\setlength{\evensidemargin}{2.5cm}     %% offset des pages paires
%\setlength{\topmargin}{0.5cm}
%\setlength{\headheight}{0.5cm}
%\setlength{\headsep}{1cm}
%\setlength{\footskip}{1.5cm}
\setlength{\voffset}{-1in}
\setlength{\hoffset}{-0.5in}


\begin{document}
\SweaveOpts{concordance=TRUE}

\title{\'Ecole-chercheurs ASPEN 2013\\ TP3: exploration par métamodélisation} 

\SweaveOpts{engine=R}
%\VignetteIndexEntry{A quick guide to PLANOR}
\author{Faivre R.\\[2mm] 
  INRA, UR 875, Unit\'e MIA-toulouse \\ 
  Math\'ematiques
et Informatique Appliqu\'ees \\ F31326 Castanet-Tolosan Cedex\\ France}
\date{\today}
\maketitle
\tableofcontents
\medskip


\section*{Introduction}

Ce TP est découpé en 3 phases : 
\begin{itemize}
\item une phase visualisations, 
\item une phase d'utilisation de modèles de régression paramétrique ou nonparamétrique, unidimensionnel ou multidimensionnel,
\item une phase d'analyse et de recherche d'un métamodèle
\end{itemize}


%================================================================================
\subsection*{Préambule R}
%================================================================================
Il est nécessaire d'avoir accès aux librairies suivantes : mgcv, randtoolbox, ECmexico2012, akima, rgl (si possible).

ECmexico2012 contenant les modèles et les informations environnantes ainsi qu'un ensemble d'utilitaires (entre autres, perspPlus, plmm).

<<keep.source=TRUE>>=
    library(akima)
    library(rgl)
    library(randtoolbox)
    library(mgcv)
     source("plmm-mtk.R") # Si non inclus dans la librairie
    library(ECmexico2012, lib.loc = "/home/faivre/templibR")
@ 

Je vous propose de travailler sur le modèle fungus mais tout est applicable sur un autre modèle si vous le préférez.

%%%%
\section{Visalisations}
%%%%
\begin{enumerate}
\item %\paragraph{Question 1.1} 
Simuler un échantillon selon un plan LHS sur tous les facteurs du modèle fungus et visualiser les points échantillonnés. Qu'observe-t-on ?

Afin de voir facilement la propriété majeure des plans LHS, un plan de taille 10 suffit. Pour cela, utilisation des fonctions lhs.plan, plot et attach pour avoir accès directement au nom des variables. 


<<keep.source=TRUE>>=
set.seed(1999)

# UTILISATION DE LHS.PLAN
#
# lhs.plan ( taille , plage = xxx.factors)
# 
lhs.plan(10, plage = fungus.factors)

fung.plan = lhs.plan(10, plage = fungus.factors) 

plot(fung.plan)

attach(fung.plan) # permet de pouvoir utiliser directement les noms des facteurs

ls(pos=2) # pour vérifier les noms auxquels on a accès directement 
# (attention si ces noms sont déjà utilisés)

plot(Tmin,Topt)
@ 

Pour voir le découpage, on peut utiliser abline (v = ; h = ).

<<keep.source=TRUE>>=
abline(v=seq(fungus.factors["Tmin","binf"], fungus.factors["Tmin","bsup"],length=11), lty=2)
# 11 = 10 (nombre de points) + 1
abline(h=seq(fungus.factors["Topt","binf"], fungus.factors["Topt","bsup"],length=11), lty=2)
@ 


\item [1b.] Tester la commande suivante. Qu'observe-t-on ? A quoi (entre autres) doit-on être attentif quand on fait des graphiques ?

<<keep.source=TRUE>>=
plot(Tmin, Topt, xlim=unlist(fungus.factors["Tmin",c("binf","bsup")]), 
 ylim=unlist(fungus.factors["Topt",c("binf","bsup")])) 
detach() # pour ne plus avoir accès aux variables directement
@ 

\item %\paragraph{Question 1.2} 
Simuler les sorties du modèle aux températures 10, 14 et 18 pour des facteurs échantillonnés selon un plan LHS de taille 50. Comparer les différents modes de visualisation en perspective, image, contour... 

On peut comparer les visualisations en perspective, sous  forme d'image ou en coutour (option type de perspPlus).  "Jouer avec perspPlus" pour l'aspect angulaire, (options pphi, ptheta) pour la visualisation de surface en perspective (option par défaut de perspPlus).
On peut également visualiser la surface à l'aide d'un graphique dynamique (librairie rgl, option type=4 de perspPlus).
Utilisation de lhs.plan, fungus.simule et perspPlus. Accessoirement de attach et detach (pour ceux qui veulent avoir accès directement aux noms des variables) ainsi que dev.new,  dev.next, dev.off et graphics.off,  pour avoir plusieurs fenêtres graphiques et les détruire en session interactive. 

Faire args(fungus.simule) pour voir les différents arguments de la fonction fungus.simule et utilisation de la fonction perspPlus.


<<keep.source=TRUE>>=
fung.plan = lhs.plan(50, plage=fungus.factors)
args(fungus.simule)
sortie.fung = fungus.simule(fung.plan, temperature = c(10,14, 18), tout=TRUE)
sortie.fung[1:10,] # on visualise les 10 premières simulations

## On note que les sorties sont appelées T.valeur

attach(sortie.fung)

perspPlus(Tmin, Topt, T.10, nomx="Tmin", nomy="Topt",nomz="Durée d'humectation")
perspPlus(Tmin, Topt, T.10, nomz="Durée d'humectation", type=2)
perspPlus(Tmin, Topt, T.10, nomz="Durée d'humectation", type=3)

## On peut utiliser par (mfrow=c(1,3),pty="s") pour pouvoir les comparer

oldpar = par()
par (mfrow=c(1,3),pty="s")
perspPlus(Tmin, Topt, T.10, nomx="Tmin", nomy="Topt",nomz="Durée d'humectation")
perspPlus(Tmin, Topt, T.10, nomz="Durée d'humectation", type=2)
perspPlus(Tmin, Topt, T.10, nomz="Durée d'humectation", type=3)
par(oldpar)

## Pour avoir un seul graphique par fenêtre, faire dev.new() 
## et dev.off() pour détruire la fenêtre active.

# dev.new() ; 
perspPlus(Tmin, Wmin, T.14, pcol=c("yellow","red")) 
# dev.off()
# dev.new() ; 
perspPlus(Tmin, Wmin, T.14, pcol=c("yellow","red"),pphi=50)
# dev.off()
# dev.new() ; 
perspPlus(Tmin, Wmin, T.14, ptheta=-10)
# dev.off()
# graphics.off() pour les détruire toutes
@ 

\item [2b.]
Pour ceux qui ont accès à la librairie rgl, faire un graphique en 3D dynamique (type=4) et positionner les points sur la surface. Que constate-t-on ? Pourquoi certains points ne sont pas sur la surface ?

<<keep.source=TRUE>>=
perspPlus(Tmin, Topt, T.10, nomx="Tmin", nomy="Topt", nomz="Durée d'humectation",
 type=4)
points3d(Tmin, Topt, T.10,pch=21,col="red")
spheres3d(Tmin, Topt, T.10,col="grey", radius = 0.03)

rgl.postscript("figTP3.eps", fmt="eps", drawText=TRUE ) 
## pour créer un fichier eps avec le bon angle de vue.
@ 

\item %\paragraph{Question 1.3} 
Tracez la surface obtenue sur un échantillon LHS de 50 valeurs puis de 500 valeurs et de 5000 tirages avec le triplet Tmin, Topt et T.10. Quel est l'effet de la taille de l'échantillon, voit-on les mêmes chose sur les trois types de graphiques. Untiliser en interactif dev.new() pour que les graphiques soient dans des fenêtres différentes.

<<keep.source=TRUE>>=
perspPlus(Tmin, Topt, T.10)
title(main = "LHS de 50")
detach(sortie.fung)

fung.plan1 = lhs.plan(500, plage=fungus.factors)
sortie1.fung = fungus.simule(fung.plan1, temperature = c(10,14, 18), tout=TRUE)

attach(sortie1.fung)
perspPlus(Tmin, Topt, T.10)
title(main = "LHS de 500")
detach(sortie1.fung)

fung.plan2 = lhs.plan(5000, plage=fungus.factors)
sortie2.fung = fungus.simule(fung.plan2, temperature = c(10,14, 18), tout=TRUE)

attach(sortie2.fung)
perspPlus(Tmin, Topt, T.10)
title(main = "LHS de 5000")
detach(sortie2.fung)
@

\item %\paragraph{Question 1.4} 
Tracez la surface obtenue en ne faisant varier que 2 paramètres (Tmin et Topt par exemple sur le modèle fungus) et en utilisant pour les autres facteurs leur valeur par défaut.  Qu'observe-t-on ? Comment interpréter ces comportements ?

Utilisation de expand.grid, fungus.simule et perspPlus. Accessoirement de attach et detach (pour ceux qui veulent avoir accès directement aux noms des variables) ainsi que dev.new,  dev.next, dev.off et graphics.off,  pour avoir plusieurs fenêtres graphiques et les détruire ensuite. 

On ne va faire un plan que pour les variables Tmin et Topt.

<<keep.source=TRUE>>=
fung.plan0 = expand.grid(Tmin = fung.plan[,"Tmin"], Topt=fung.plan[,"Topt"],
  Tmax=20,Wmin=4.5,Wmax=9.5)
sortie0.fung = fungus.simule(fung.plan0, temperature = c(10,14, 18), tout=TRUE)

attach(sortie0.fung)
perspPlus(Tmin, Topt, T.10)
detach(sortie0.fung)
@ 
\end{enumerate}

%%%%
\section{Modèles de régression paramétrique ou nonparamétrique}
%%%%

Objectif : se familiariser avec quelques fontions de régression paramétrique et non-paramétrique.

Dans cette partie, on travaillera sur le modèle fungus, à la température 14. Il s'agira principalement de visualiser les courbes estimées de la sortie en fonction des valeurs du seul facteur Topt, puis du couple de facteurs Topt et Wmin pour les surfaces de réponse estimées.

On reste sur l'attachement sur le fichier SORTIE (ou le nom que vous avez choisi) sinon on refait attach(SORTIE) : un LHS de taille 50 suffit.  On a des informations en faisant search() et ls(pos=2) si SORTIE est en position 2.

<<keep.source=TRUE>>=
search()
attach(sortie.fung)
@ 

Nous allons voir successivement les  fonctions ksmooth, loess et gam pour les régressions non paramétriques et lm (avec poly et polym) pour les régressions paramétriques. Les fonctions loess, gam, lm, \dots,  sous R,  générent des résultats spécifiques mais qui d'un point de vue utilisateur R vont faire appel aux mêmes fonctions d'analyse et de visualisation. Les fonctions plot, summary, predict, \dots, sont génériques. 

Pour chaque question, vous pouvez utiliser le script de la solution pour aller plus vite si vous le souhaitez.

\begin{enumerate}
\item Tracez sur le même graphique les courbes lissées obtenues par un estimateur à noyau en modifiant le type de noyau ainsi qu'en jouant sur la largeur de la bande glissante. Travaillez sur la variable explicative Topt et la variable de sortie T.14

ksmooth(ksmooth(x,y)) ; lines(ksmooth(x1,res)) options kernel="normal" ou bandwidth= valeur

<<keep.source=TRUE>>=
plot(Topt,T.14)
lines(ksmooth(Topt,T.14))
lines(ksmooth(Topt,T.14, kernel="normal"),col=2)
lines(ksmooth(Topt,T.14,bandwidth=1),col=3)
lines(ksmooth(Topt,T.14,bandwidth=3),col=4)
lines(ksmooth(Topt,T.14,bandwidth=.1),col=5)
@ 


\item Estimez par régression polynomiale locale la relation  et comparer là avec la courbe estimée par un lisseur à noyau. Modifiez le degré de lissage.

loess (y $ \sim $ x, span=) ; predict(resultat de l'estimation, nouvelles donnees).
Utilisation de loess, data.frame et predict.


loess : loess (V2 $ \sim $ Topt) ;  lines ( newdata\$x1, predict(ishloess,newdata), lty=1, lwd=2)


<<keep.source=TRUE>>=
fung.loess = loess (T.14 ~ Topt)
fung.loess1 = loess (T.14 ~ Topt, span=1)
plot(Topt,T.14)
newdata.Topt = data.frame(Topt=seq(8,10,len=101)) # valeurs où on va prédire
predict.loess = predict(fung.loess,newdata=newdata.Topt)
predict.loess1 = predict(fung.loess1,newdata=newdata.Topt)
lines(newdata.Topt$Topt,predict.loess)
lines(newdata.Topt$Topt,predict.loess1, col=2)

# On peur superposer ce graphique avec les ksmooth
lines(ksmooth(Topt,T.14, kernel="normal"),col=3)
lines(ksmooth(Topt,T.14, kernel="normal", bandwidth=1),col=4)
@ 

\item Estimez la courbe expliquant T.14 par régression linéaire simple puis polynomiale (jusqu'au degré 4) sur la variable Topt. Tracez les courbes estimées sur le même graphique.

 lm(y $ \sim $ x) ; lm(y $ \sim $ poly(x,degre))

<<keep.source=TRUE>>=
fung.lm = lm(T.14 ~ Topt)
summary(fung.lm)

cat("Ne pas oublier de faire RETURN dans la fenêtre de commande sous Linux\n")

oldpar = par()
par(mfrow = c(2, 2), oma = c(0, 0, 2, 0))
plot(fung.lm)
par(oldpar)

plot(Topt,T.14)
lines(newdata.Topt$Topt,predict(fung.lm,newdata=newdata.Topt)) 

# on peut utiliser la même structure de données que précédemment (structure newdata) 
# car elles sont indépendantes du modèle

fung.lm2 = lm(T.14 ~ poly(Topt,2))
summary(fung.lm2)
lines(newdata.Topt$Topt,predict(fung.lm2,newdata=newdata.Topt), col=2)
fung.lm3 = lm(T.14 ~ poly(Topt,3))
summary(fung.lm3)
lines(newdata.Topt$Topt,predict(fung.lm3,newdata=newdata.Topt), col=3)
fung.lm4 = lm(T.14 ~ poly(Topt,4))
summary(fung.lm4)
lines(newdata.Topt$Topt,predict(fung.lm4,newdata=newdata.Topt), col=4)
@ 

\item Estimez par un modèle additif généralisé la relation entre T.14 et Topt en utilisant des splines de régression. 

Utilisation de gam, s, predict :  gam( y $ \sim $ s(x, bs= ))

<<keep.source=TRUE>>=
fung.gam = gam( T.14 ~ s(Topt))
summary(fung.gam)

plot(fung.gam)

plot(Topt,T.14)
lines(newdata.Topt$Topt,predict(fung.gam,newdata=newdata.Topt)) 
@ 


\item Estimez par régression polynomiale locale multiple la surface de réponse correspondant au modèle expliquant T.14 par les deux variables explicatives Topt et Wmin.

Utilisation de loess, expand.grid pour générer les nouvelles données sur lesquelles on veut prédire et predict.

 loess (T.14 $ \sim $ Topt+Wmin), 

<<keep.source=TRUE>>=
fung.loess2 = loess (T.14 ~ Topt+Wmin)
summary(fung.loess2)

newdata.ToptWmin = expand.grid(Topt=seq(8,10,len=101),Wmin=seq(4,5,len=11))

predict.loess2 = predict(fung.loess2, newdata=newdata.ToptWmin)
perspPlus(seq(8,10,len=101), seq(4,5,len=11), predict.loess2, 
       nomx="Topt",nomy="Wmin",nomz="Durée d'humectation")

## 8-10 et 4-5 sont les intervalles de variation des facteurs Topt et Wmin

perspPlus(seq(8,10,len=101), seq(4,5,len=11), predict.loess2, 
     nomx="Topt",nomy="Wmin",nomz="Durée d'humectation",type=4)
points3d(Topt,Wmin,T.14, col=3)
spheres3d(Topt, Wmin, T.14,col="grey", radius = 0.03)
@ 

\item Procédez à l'estimation de la surface de réponse de T.14 par un modèle de régression linéaire multiple de degré 1 à 4 (variables explicatives Topt et Wmin).

Utilisation de polym(x1,x2), (et éventuellement de for pour aller plus vite).

<<keep.source=TRUE>>=
for(i in 1:4) {
# dev.new()
fung.multlm = lm (T.14 ~ polym(Topt,Wmin,degree=i))

predict.multlm = predict(fung.multlm, newdata=newdata.ToptWmin)

perspPlus(seq(8,10,len=101),seq(4,5,len=11), matrix(predict.multlm,ncol=11),
  nomx="Topt",nomy="Wmin",nomz="Durée d'humectation")
title(main= paste("degré",i))
}
summary(fung.multlm)

# dev.new()
perspPlus(Topt,Wmin,T.14)

# graphics.off() pour détruire toutes les fenêtres graphiques

# il faut toujours faire attention au format de sortie des fonctions (dim(predict.multlm))
@ 


\item Estimez par un modèle additif généralisé la relation entre T.14 et Topt et Wmin en utilisant des splines de régression. Comparez le modèle somme de la décomposition des deux variables explicatives avec la décomposition bidimensionnelle.


gam(y $ \sim $ s(x1,x2))
Utilisation de gam, s, predict, anova.
<<keep.source=TRUE>>=
fung.gam2 = gam( T.14 ~ s(Topt,Wmin))
plot(fung.gam2)
summary(fung.gam2)

predict.gam2 = predict(fung.gam2, newdata=newdata.ToptWmin)
perspPlus(seq(8,10,len=101),seq(4,5,len=11),matrix(predict.gam2,ncol=11), 
  nomx="Topt",nomy="Wmin",nomz="Durée d'humectation")

ish.plan = lhs.plan(50, plage=ishigami.factors)
sortie.ish = ishigami.simule(ish.plan, tout=TRUE)
ish.gam2 = gam(res~ s(x1,x2),data=sortie.ish)
plot(ish.gam2)

fung.gam3 = gam( T.14 ~ s(Topt)+s(Wmin))

cat("Ne pas oublier de faire RETURN dans la fenêtre de commande sous Linux\n")

plot(fung.gam3, pages=1)
summary(fung.gam3)

predict.gam3 = predict(fung.gam3, newdata=newdata.ToptWmin)

perspPlus(seq(8,10,len=101),seq(4,5,len=11),matrix(predict.gam3,ncol=11), nomx="Topt",nomy="Wmin",nomz="Durée d'humectation")

perspPlus(seq(8,10,len=101),seq(4,5,len=11),matrix(predict.gam2,ncol=11), 
  nomx="Topt",nomy="Wmin",nomz="Durée d'humectation")

detach(sortie.fung)
@ 
\end{enumerate}

%%%%
\section{Recherche d'un métamodèle}
%%%%

Nous allons rechercher, sur  le modèle fungus, le maximum d'information sur sa structure et éventuellement s'il est possible de réduire sous une forme ``manipulable'' comme un modèle de régression linéaire. 

En règle générale, les analyses faites dans les TP1 et TP2 (Morris, E-Fast, Sobol, \dots) permettent de proposer un ``grand modèle'' dit ``modèle complet'' (en fait elles indiquent l'ensemble des facteurs d'intérêt influants en effet principal ou en interaction), à partir duquel les procédures statistiques de choix de modèle permettent de le réduire ou d'affiner les relations.

Lorsqu'on explore un modèle avec un grand nombre de facteurs (paramètres), on peut difficilement procéder de la sorte et en règle générale, on part des effects principaux et on complète le modèle par des interactions entre facteurs pour pouvoir expliquer la plus grande part de variabilité du modèle. 

Dans ce TP, nous avons la possibilité de partir d'un modèle dit complet avec toutes les interactions entre les facteurs sous la forme d'un modèle de régression polynomiale multiple. L'objectif sera ici de trouver un modèle ``plus simple'' qui permette d'expliquer  99 \% au moins de la variabilité du modèle initial. Nous allons voir si on peut retrouver certaines caractéristiques du modèle fungus.

Etant donné qu'on va procéder à cette recherche sur la base d'un plan d'expérience simulé (LHS), les jeux de données pourraient être très différents selon les tirages. Afin de facilier l'aide des assistants des TP, il est préférable, au début, de travailler sur le même jeu de données. Pou ce faire, on utilisera la même amorce de simulation. On utilisera un échantillon de taille 1000 plus propice à rendre estimable un plus grand nombre d'interactions. On ne travaillera par la suite que sur la température de 14 degré.

<<keep.source=TRUE>>=
set.seed(2010)
fung.plan2 = lhs.plan(1000, plage=fungus.factors)
sortie2.fung = fungus.simule(fung.plan2, temperature = c(10,14, 18), tout=TRUE)
attach(sortie2.fung)
@ 

\begin{enumerate}
\item Démarrez la recherche d'un modèle simplifié avec un modèle linéaire contenant toutes les variables explicatives du modèle fungus en interaction polynomiale d'ordre 3. Analysez les résultats. Que constatez-vous ?

Utilisation de plmm.mtk, summary.plmm et plot.plmm 

<<keep.source=TRUE>>=
fung.plmm3 = plmm.mtk(sortie2.fung[,c("Tmin","Topt","Tmax", "Wmin", "Wmax", "T.14")],
  degpol = 3)
summary.plmm(fung.plmm3)
plot.plmm(fung.plmm3)
@
ou plus classiquement avec lm, polym, summary.
{\small
<<keep.source=TRUE>>=
fung.iden = lm ( T.14 ~ polym(Tmin,Topt,Tmax, Wmin, Wmax, degree=3))
options(width=120) # si on veut des lignes plus longues
summary(fung.iden)
@
 }

\item A l'aide du résumé de l'analyse de régression linéaire (lm sur polym), est-ce qu'une variable n'intervient pratiquement jamais dans les effets significatifs ? Si oui, laquelle. Est-ce en cohérence avec les conclusions de l'analyse avec plmm.mtk ?

Remarque 1 :  Comme on a utilisé polym avec les options par défaut, on peut recalculer les indices de sensibilité obtenus avec plmm (les sommes de carrés sont toutes indépendantes).

Remarque 2 : Si un facteur intervient avec un degré supérieur, on va sous estimer l'effet de ce facteur. C'est la résiduelle globale qui va nous dire si l'approximation  est acceptable quand on considère que fungus s'exprime comme un polynome multiple de degré 3.

On remarque que le facteur Wmax n'intervient significativement que pour certaines interactions avec d'autres facteurs (d'ordre au moins 3), mais pas du tout en effect direct. Pour cela, on regarde la 5-ième composante (toutes celles qui n'ont pas de 0 à la fin) :

polym(Tmin, Topt, Tmax, Wmin, Wmax, degree = 3)0.0.0.0.1 


Les interactions significatives obtenues sont 1.1.0.0.1, 1.0.1.0.1 et 0.1.0.0.2 et quelques traces pour d'autres interactions. 

C'est l'interprétation première que l'on fait en visualisant les résultats de l'analyse plmm.

\item Estimez le même modèle en ôtant la variable qui n'est pas considérée comme influente et comparez les deux modèles obtenus. Peut-on accepter de ne pas la prendre en compte ? 

Utilisation de anova(modele1,modele2) qui permet de comparer deux modèles

Pour comparer, soit on fait les calculs ``à la main'', soit on fait
<<keep.source=TRUE>>=
fung.iden2 = lm ( T.14 ~ polym(Tmin,Topt,Tmax, Wmin, degree=3)) # on a enlevé Wmax
summary(fung.iden2)
anova(fung.iden2,fung.iden) # compare deux modélisations (on retrouve l'effet total de Wmax)
@

On ne rejette pas l'hypothèse ``l'effet de Wmax est nul''.

\item On continue d'analyser le modèle avec cette variable en moins. Quel est  le point commun des composantes non significatives. Comment peut-on prendre cela en compte dans la simplification du modèle ? Accepte-t-on cette nouvelle hypothèse ? Où en sommes-nous des 99 \% de variance expliquée ?


Utilisation de polym( raw=TRUE) et de l'interaction $A:B$. En effet, l'utilisation de $*$ pour décrire les interactions entre deux variables comportent également la description des effets principaux.

On constate sur le résultat précédent que les termes contenant Wmin avec un degré supérieur à 1 sont quasi non significatifs. Testons un effet de Wmin multiplicatif.


<<keep.source=TRUE>>=
fung.iden3 = lm ( T.14 ~ polym(Tmin,Topt,Tmax, degree=3)*Wmin)
summary(fung.iden3) 
anova(fung.iden2,fung.iden3)
@

On peut remarquer que Wmin intervient en effet multiplicatif et que pratiquement tous les premiers termes d'effet moyen (sans Wmin) sont nuls. 

Le modèle ``lm ( T.14 $\sim$ polym(Tmin,Topt,Tmax, degree=3)*Wmin)'' peut s'écrire :

fungus = Moyenne + polym(Tmin,Topt,Tmax, degree=3) + polym(Tmin,Topt,Tmax, degree=3):Wmin

On regarde tout d'abord uniquement l'interaction. On constate qu'il reste ``de l'Intercept'' dû aux centrages.

{\small 
<<keep.source=TRUE>>=
fung.iden4 = lm ( T.14 ~ polym(Tmin,Topt,Tmax, degree=3):Wmin)
summary(fung.iden4) 
@
}

Pour ne pas recentrer, on peut utiliser les polynomes orthogonaux bruts.

{\small 
<<keep.source=TRUE>>=
fung.iden4raw = lm ( T.14 ~ polym(Tmin,Topt,Tmax, degree=3, raw=TRUE):Wmin)
summary(fung.iden4raw) 
@
}

On enlève l'Intercept non significatif.

{\small 
<<keep.source=TRUE>>=
fung.iden4rawm = lm ( T.14 ~ -1 + polym(Tmin,Topt,Tmax, degree=3, raw=TRUE):Wmin)
summary(fung.iden4rawm)
@
}

\item Peut-on encore simplifier ? Vous pouvez maintenant regarder le code de la fonction fungus.model. Qu'a-t-on retrouvé ?

On peut continuer à simplifier (surtout avec notre critère de 99 \%) par exemple avec 
lm ( T.14 $\sim$ polym(Tmin,Topt,Tmax, degree=2, raw=TRUE):Wmin) mais on voit qu'on commence à perdre.

{\small 
<<keep.source=TRUE>>=
summary(lm ( T.14 ~ -1 + polym(Tmin,Topt,Tmax, degree=2, raw=TRUE):Wmin))
@
}


Il faudrait reprendre la modélisation sur la base d'un modèle

{\small 
<<keep.source=TRUE>>=
summary(lm ( T.14/Wmin ~ polym(Tmin,Topt,Tmax,Wmax, degree=3)))
@
}

\end{enumerate}

%%%%
\end{document}
%%%%

