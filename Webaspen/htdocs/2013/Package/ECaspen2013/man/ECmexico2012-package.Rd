\name{ECaspen2013-package}
\alias{ECaspen2013-package}
\alias{ECaspen2013-package}
\title{Librairie R de l'ECOLE-CHERCHEURS ASPEN, LES HOUCHES, 8-12 avril 2013}
\description{Librairie R de l'ECOLE-CHERCHEURS ASPEN, LES HOUCHES, 8-12 avril 2013}
\details{\tabular{ll}{
Package: \tab ECaspen2013\cr
Type: \tab Package\cr
Version: \tab 1.0-0\cr
Date: \tab 2013-03-23\cr
Depends: \tab R (>= 1.8.0), sensitivity, akima, car, evd, lhs, mgcv, rgl, triangle\cr
License: \tab GPL\cr
LazyLoad: \tab yes\cr
Collate: \tab 'ECaspen2013.R' 'TPLibModels.R' 'TPLibUtile.R' 'TPLibData.R'\cr
Encoding: \tab latin1\cr
}}
\docType{package}
\author{ec-aspen, maintainer: Hervé Monod}
\alias{ECaspen2013}
\alias{ECaspen2013-package}

