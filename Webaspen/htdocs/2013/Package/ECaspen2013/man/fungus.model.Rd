\name{fungus.model}
\alias{fungus.model}
\title{Modele "fungus" de croissance d'un champignon selon l'humidite}
\usage{fungus.model(param=fungus.factors$nominal, temperature=10)
}
\description{Modèle "fungus" (croissance champignon selon l'humidité)}
\arguments{\item{param}{vecteur de longueur 5 comprenant un jeu de valeurs de Tmin, Topt, Tmax, Wmin, Wmax}
\item{temperature}{scalaire ou vecteur de temperatures}
}
\value{scalaire ou vecteur de longueur egale au nombre de temperatures}
\details{Fungus est un modèle générique pour calculer la durée
  d’humectation \eqn{Y} (en heures) nécessaire au développement d'un
  champignon (Wetness), pour une température moyenne \eqn{T} donnée (en
  degrés centigrades)
  (Magarey et al., 2005). L'équation du modèle est \deqn{Y =
    \min(W(T),W_\mathrm{max})}{Y = min(W(T), Wmax)} avec
\deqn{W(T) =
  \frac{W_\mathrm{min}}{\frac{T_\mathrm{max}-T}{T_\mathrm{max}-T_\mathrm{opt}}\times\frac{T-T_\mathrm{min}}{T_\mathrm{opt}-T_\mathrm{min}}}}{W(T) = Wmin / [ (Tmax - T)/(Tmax - Topt) x (T - Tmin)/(Topt - Tmin) ]}.}
\note{Des valeurs min, max et nominales des paramètres sont données
dans fungus.factors. Elles correspondent à \emph{Alternaria
Brassicae}, ravageur du colza (Magarey et al, 2005).}
\references{Magarey RD, Sutton TB, Thayer CL (2005). A simple generic infection
model for foliar fungal plant pathogens. Phytopathology 95, 92-100.}
\examples{fungus.model( fungus.factors$nominal, temperature=c(10,15,18,21,30) )}

