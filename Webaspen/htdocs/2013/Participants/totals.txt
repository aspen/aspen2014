1	Berre	      	       Loïk		      	Météo-France											
2	Blayo	      	       Eric			University of Grenoble, France							
3	Bocquet	      	       Marc			Ecole des Ponts ParisTech, France						
4	Brasseur      	       Pierre			CNRS, LEGI, Grenoble, France							
5	Cardinali     	       Carla			ECMWK, Reading, UK								
6	Cosme	      	       Emmanuelle		University of Grenoble, France							
7	Chevallier    	       Frédéric			LSCE, France									
8	Debreu	      	       Laurent			INRIA, Grenobale, France							
9	Desroziers    	       Gérald			Météo-France									
10	Elbern	      	       Hendrik			University of Cologne, Germany							
11	Fisher	      	       Mike			ECMWF, Reading, UK								
12	Fukumori      	       Ichiro			JPL, USA									
13	Hascoët       	       Laurent			INRIA, Sophia-Antipolis, France							
14	Hanea	      	       Remus			Delft University/TNO, The Netherlands						
15	Hotekamer     	       Peter			Environment Canada								
16	Ide	      	       Kayo			University of Maryland, USA							
17	van Leeuwen   	       Peter Jan		University of Reading, UK							
18	Le Dimet      	       François-Xavier		University of Grenoble, France							
19	Lemieux-Dudon 	       Bénédicte		University of Grenoble, France							
20	Lorenc	      	       Andrew			MeteOffice, Leceister, UK							
21	Mallet	      	       Vivien			INRIA, Paris, France								
22	Nodet	      	       Maëlle			University of Grenoble, France							
23	Rabier	      	       Florence			Metéo-France									
24	Snyder	      	       Chris			NCAR, USA									
25	Talagrand     	       Olivier			CNRS, LMD, France								
26	Verron	      	       Jacques			CNRS, LEGI, Grenoble, France							
27	Vidard	      	       Arthur			INRIA, Grenobale, France							
28	Zavala-Garay  	       Javier			Rutgers University, USA   

1	Autret 	      	       Emmanuelle		Ifremer, Brest, France								
2	Barthélemy    	       Antoine			Université catholique de Louvain, Belgium					
3	Bell	      	       Simon			Aston University, Birminghan, UK						
4	Bellsky	      	       Tom			Arizona State University, USA							
5	Benavides     	       Simon 			LSCE, France									
6	Berchet	      	       Antoine 			LSCE, France									
7	Bianchi	      	       Blandine 		EPFL, Switzerland								
8	Bonan 	      	       Bertrand			LJK, Grenoble, France								
9	Bouttier      	       Pierre-Antoine 		LJK, Grenoble, France								
10	Bucanek       	       Antonin 			Czech Hydrometeorological Institute, Czeck Republic				
11	Buchard-Marcha	       Virginie 		NASA/USRA GSFC, USA								
12	Carley	      	       Jacob 			Purdue University, USA								
13	Chabot 	      	       Vincent 			LJK, Grenoble, France								
14	Chatterjee    	       Abhishek			University of Michigan, USA							
15	Diniz	      	       Fabio 			CPTEC/INPE, Brazil								
16	Dreano	      	       Denis			KAUST, Saudi arabia								
17	El Gharamti   	       Mohamad			KAUST, Saudi Arabia								
18	Eyyammadichi  	       Divya 			Indian Institute of Tropical Meteorology, Pahan, Pune, India			
19	Fekri	      	       Majid 			McGill University, Canada							
20	Gaubert	      	       Benjamin 		LISA, France									
21	Geppert	      	       Gernot 			Max Planck Institute for Meteorology, Hamburg, Germany				
22	Haslehner     	       Mylene 			Hans-Ertel-Centre for Data Assimilation, Munich, Germany			
23	Hu	      	       Huiqin 			Dpt of Atm. & Oc. Sc., Peking University					
24	Huang	      	       Ling 			Dpt of Atm. & Oc. Sc., Peking University					
25	Igri	      	       Moudi Pascal		University of Yaounde, Cameroon							
26	Ito	      	       Kosuke			National Taiwan University							
27	Kay	      	       Jun Kyung		Yonsei University, Seoul, South Korea						
28	Kizhakkeniyil 	       Manoj			University of Northern British Columbia, Canada					
29	Lee	      	       Ite 			Ionospheric Radio Science Laboratory, Jhongli City, Taiwan and NCAR		
30	Lee	      	       Jin 			Centre for Australian Weather and Climate Research, Australia			
31	Maßmann       	       Silvia			BSH, Hamburg, Germany								
32	Mechri	      	       Rihab 			LSCE, France									
33	Mongin	      	       Mathieu 			CSIRO, Hobart, Australia							
34	Mostamandi    	       Suleyman			Russian State Hydrometeorological University, St. Petersburg, Russia	
35	Navarro       	       Thomas 			LMD, France									
36	Osychny	      	       Vladimir			NCEP, Maryland, USA								
37	Pentakota     	       Sreenivas		Indian Institute of Tropical Meteorology, Pune, India				
38	Rong	      	       Xinyao			Chinese Academy of Meteorological Sciences, Biejing, China			
39	Ruggiero      	       Giovanni 		University of Nice-Sophia-Antipolis, France					
40	Sana	      	       Furrukh			KAUST, Saudi arabia								
41	Saucedo       	       Marcos			CIMA/CONICET-UBA, Buenos Aires, Argentina					
42	Shlyaeva      	       Anna 			Hydrometeorological Research Center of Russia, Moscow, Russia			
43	Sirisup	      	       Sirod			National Institute of Informatics, Tokyo, Japan					
44	Slivinski     	       Laura 			Brown University, Providence, USA						
45	Sommer	      	       Matthias 		Hans Ertel Center for Data Assimilation, Munich, Germany			
46	Sperrevik     	       Ann Kristin		Norwegian Meteorological Institue, Oslo, Norway					
47	Toque	      	       Nathalie			CMCC-INGV, Bologna, Italy							
48	Vrettas       	       Michail 			Aston University, Birmingham, UK						
49	Wang	      	       Yiguo 			LSCE/CEREA, France								
50	Wecht	      	       Kevin			Harvard University, USA								
51	Wilson	      	       Gregory			Oregon State University, USA							
52	Winiarek      	       Victor 			Ecole des Ponts ParisTech, France						
53	Wu	      	       Shu			Center for Climatic Research, Wisconsin, USA				
54	Yue	      	       Jian 			Peking University, Beijing, China 						
55	Zhao          	       Shunliu			Carleton University, Ottawa, Canada							
