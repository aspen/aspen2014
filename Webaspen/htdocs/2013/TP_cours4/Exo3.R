rm(list=ls(all.names=TRUE))
library(lhs)
library(DiceKriging)

######################## BASE D'APPRENTISSAGE et BASE de TEST ##########################
N_BA = 80;                ## Nombre de points de la base d'apprentissage
d = 2;                     ## Dimension de l'espace des entrees
S_BA = .....              ## Simulation de la BA par plan LHS optimis� => utilisation de la fx optimumLHS

                           ## S_BA : entrees
                           ## Y_BA : sortie du simulateur
N_BT_dim = 70;
N_BT = N_BT_dim^2;               ### Nombre de points de la base de test
S_BT_dim <- seq(0,1,length.out=N_BT_dim)
S_BT <- expand.grid(S_BT_dim,S_BT_dim) ### Simulation d'une premiere base de test
                           ## S_BT : entrees de la base de test
                           ## Y_BT : sortie du simulateur

schwefel <- function(x){
	x2 <- x*400-200
	rowSums(-x*sin(sqrt(abs(x2))))
}

Y_BA <- schwefel(S_BA)
Y_BT <- schwefel(S_BT)

par(mfrow=c(1,1))
filled.contour(S_BT_dim,S_BT_dim,matrix(Y_BT,N_BT_dim,N_BT_dim),main='Function Schwefel',xlab='X1',ylab='X2',color.palette=heat.colors,
plot.axes={points(S_BA[,1],S_BA[,2],pch=3)})



######################### METAMODELING  #############################
metamodel <- ...  # Creation du metamodele processus gaussien => utilisation de la fx km

# ###### Prediction sur la BT avec modele PG #######
predictions_BT<- .... # Pr�diction du metamodele sur la base de test => utilisation de la fx predict.km

filled.contour(S_BT_dim,S_BT_dim,matrix(predictions_BT$mean,N_BT_dim,N_BT_dim),xlab='X1',ylab='X2',main='Pr�diction du m�tamod�le',color.palette=heat.colors) 
filled.contour(S_BT_dim,S_BT_dim,matrix(abs(Y_BT - predictions_BT$mean),N_BT_dim,N_BT_dim),xlab='X1',ylab='X2',main='Erreur du m�tamod�le PG (valeur absolue)',color.palette=heat.colors) 

# ###### Calcul du Q2 sur la BT #########
# Creation de la fonction qui permet de calculer le Q2
Q2 <- function(y,yhat)
  1-mean((y-yhat)^2)/var(y)

Q2_BT = Q2(Y_BT,predictions_BT$mean)
#### Affichage des resultats ####
print(paste('Nombre de points de la base d apprentissage :',N_BA))
print(paste('Dimension de l espace des entree :',d))
print(paste('Q2 sur Base de test :',Q2_BT))









