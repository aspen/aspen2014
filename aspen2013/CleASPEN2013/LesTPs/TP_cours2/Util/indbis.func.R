#############################################################################
#
# Function:  indbis.func.R
# Purpose:   Fonction indicatrice
#      f(X)=#
#      fi(Xi)=#
#      Xi: variables aleatoires independantes uniformement distribuees su [0,1]
# 
# Author:    L. Viry
# Modified:  
# Date:      25/02/2013
#
# Entrees::
#   X : echantillon
# Sorties:
#   y : valeurs de la fonction ind sur l'echantillon X
# 
################################################################################  
indbis.func <- function (X) 
{
  # Tester la dimension de X (2)
  
   if (length(dim(X)) != 2) {stop("X doit etre de dimension 2")}

  # Tester le nombre de variables nécessaires (3)
  
  if (dim(X)[2] < 3) {stop("Le plan X doit contenir 3 variables")}
  
  result <- as.numeric(X[,1] < X[,2])*as.numeric(X[,3]^2 < X[,2])
  
   return(result)
}
    