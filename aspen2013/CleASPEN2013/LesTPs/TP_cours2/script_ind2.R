# Approche spectrale
#
#library("ECaspen2013")
library("sensitivity")
source("Util/ind.func.R")
source("Util/rbd2006.R")

# Cas de l’indicatrice a deux variables, fonction irreguliere
#
# taille e l’echantillon 
n <- 1000

# Echantillons
X1 <- data.frame(matrix(runif(2*n), nrow = n))
X2 <- data.frame(matrix(runif(2*n), nrow = n))

# Sobol2007
xsobol2007 <- sobol2007(model = ind.func, X1 = X1, X2 = X2, nboot = 100)
print(xsobol2007)
plot(xsobol2007)

# SobolEff
#
xsobolEff <- sobolEff(model = ind.func, X1 = X1, X2 = X2, nboot = 100)
print(xsobolEff)
plot(xsobolEff)

# Methode de Sobol a l'aide deux hypercubes matins repliques
#
xsobolRLHS <- function(model=ind.func,d=2,q=n,t=1,alpha=NULL)
print(xsobolRLHS)

# Random balance designs pour l'estimation des indices de Sobol d'ordre un
#   et la correction de biais
xrbd2006 <- rbd2006(model=ind.func,d=2,n,M=6,omega=1)
print(xrbd2006)