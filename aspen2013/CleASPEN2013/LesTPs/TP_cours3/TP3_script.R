    library(akima)
    library(rgl)
    library(randtoolbox)
    library(mgcv)
    library(ECaspen2013)

set.seed(1999)

# UTILISATION DE LHS.PLAN
#
# lhs.plan ( taille , plage = xxx.factors)
# 
lhs.plan(10, plage = fungus.factors)

fung.plan = lhs.plan(10, plage = fungus.factors) 

plot(fung.plan)

attach(fung.plan) # permet de pouvoir utiliser directement les noms des facteurs

ls(pos=2) # pour vérifier les noms auxquels on a accès directement 
# (attention si ces noms sont déjà utilisés)

plot(Tmin,Topt)
abline(v=seq(fungus.factors["Tmin","binf"], fungus.factors["Tmin","bsup"],length=11), lty=2)
# 11 = 10 (nombre de points) + 1
abline(h=seq(fungus.factors["Topt","binf"], fungus.factors["Topt","bsup"],length=11), lty=2)

plot(Tmin, Topt, xlim=unlist(fungus.factors["Tmin",c("binf","bsup")]), 
 ylim=unlist(fungus.factors["Topt",c("binf","bsup")])) 
detach() # pour ne plus avoir accès aux variables directement

fung.plan = lhs.plan(50, plage=fungus.factors)
args(fungus.simule)
sortie.fung = fungus.simule(fung.plan, temperature = c(10,14, 18), tout=TRUE)
sortie.fung[1:10,] # on visualise les 10 premières simulations

## On note que les sorties sont appelées T.valeur

attach(sortie.fung)

perspPlus(Tmin, Topt, T.10, nomx="Tmin", nomy="Topt",nomz="Durée d'humectation")
perspPlus(Tmin, Topt, T.10, nomz="Durée d'humectation", type=2)
perspPlus(Tmin, Topt, T.10, nomz="Durée d'humectation", type=3)

## On peut utiliser par (mfrow=c(1,3),pty="s") pour pouvoir les comparer

oldpar = par()
par (mfrow=c(1,3),pty="s")
perspPlus(Tmin, Topt, T.10, nomx="Tmin", nomy="Topt",nomz="Durée d'humectation")
perspPlus(Tmin, Topt, T.10, nomz="Durée d'humectation", type=2)
perspPlus(Tmin, Topt, T.10, nomz="Durée d'humectation", type=3)
par(oldpar)

## Pour avoir un seul graphique par fenêtre, faire dev.new() 
## et dev.off() pour détruire la fenêtre active.

# dev.new() ; 
perspPlus(Tmin, Wmin, T.14, pcol=c("yellow","red")) 
# dev.off()
# dev.new() ; 
perspPlus(Tmin, Wmin, T.14, pcol=c("yellow","red"),pphi=50)
# dev.off()
# dev.new() ; 
perspPlus(Tmin, Wmin, T.14, ptheta=-10)
# dev.off()
# graphics.off() pour les détruire toutes

perspPlus(Tmin, Topt, T.10, nomx="Tmin", nomy="Topt", nomz="Durée d'humectation",
 type=4)
points3d(Tmin, Topt, T.10,pch=21,col="red")
spheres3d(Tmin, Topt, T.10,col="grey", radius = 0.03)

rgl.postscript("figTP3.eps", fmt="eps", drawText=TRUE ) 
## pour créer un fichier eps avec le bon angle de vue.

perspPlus(Tmin, Topt, T.10)
title(main = "LHS de 50")
detach(sortie.fung)

fung.plan1 = lhs.plan(500, plage=fungus.factors)
sortie1.fung = fungus.simule(fung.plan1, temperature = c(10,14, 18), tout=TRUE)

attach(sortie1.fung)
perspPlus(Tmin, Topt, T.10)
title(main = "LHS de 500")
detach(sortie1.fung)

fung.plan2 = lhs.plan(5000, plage=fungus.factors)
sortie2.fung = fungus.simule(fung.plan2, temperature = c(10,14, 18), tout=TRUE)

attach(sortie2.fung)
perspPlus(Tmin, Topt, T.10)
title(main = "LHS de 5000")
detach(sortie2.fung)


fung.plan0 = expand.grid(Tmin = fung.plan[,"Tmin"], Topt=fung.plan[,"Topt"],
  Tmax=20,Wmin=4.5,Wmax=9.5)
sortie0.fung = fungus.simule(fung.plan0, temperature = c(10,14, 18), tout=TRUE)

attach(sortie0.fung)
perspPlus(Tmin, Topt, T.10)
detach(sortie0.fung)

search()
attach(sortie.fung)

plot(Topt,T.14)
lines(ksmooth(Topt,T.14))
lines(ksmooth(Topt,T.14, kernel="normal"),col=2)
lines(ksmooth(Topt,T.14,bandwidth=1),col=3)
lines(ksmooth(Topt,T.14,bandwidth=3),col=4)
lines(ksmooth(Topt,T.14,bandwidth=.1),col=5)

fung.loess = loess (T.14 ~ Topt)
fung.loess1 = loess (T.14 ~ Topt, span=1)
plot(Topt,T.14)
newdata.Topt = data.frame(Topt=seq(8,10,len=101)) # valeurs où on va prédire
predict.loess = predict(fung.loess,newdata=newdata.Topt)
predict.loess1 = predict(fung.loess1,newdata=newdata.Topt)
lines(newdata.Topt$Topt,predict.loess)
lines(newdata.Topt$Topt,predict.loess1, col=2)

# On peur superposer ce graphique avec les ksmooth
lines(ksmooth(Topt,T.14, kernel="normal"),col=3)
lines(ksmooth(Topt,T.14, kernel="normal", bandwidth=1),col=4)

fung.lm = lm(T.14 ~ Topt)
summary(fung.lm)

cat("Ne pas oublier de faire RETURN dans la fenêtre de commande sous Linux\n")

oldpar = par()
par(mfrow = c(2, 2), oma = c(0, 0, 2, 0))
plot(fung.lm)
par(oldpar)

plot(Topt,T.14)
lines(newdata.Topt$Topt,predict(fung.lm,newdata=newdata.Topt)) 

# on peut utiliser la même structure de données que précédemment (structure newdata) 
# car elles sont indépendantes du modèle

fung.lm2 = lm(T.14 ~ poly(Topt,2))
summary(fung.lm2)
lines(newdata.Topt$Topt,predict(fung.lm2,newdata=newdata.Topt), col=2)
fung.lm3 = lm(T.14 ~ poly(Topt,3))
summary(fung.lm3)
lines(newdata.Topt$Topt,predict(fung.lm3,newdata=newdata.Topt), col=3)
fung.lm4 = lm(T.14 ~ poly(Topt,4))
summary(fung.lm4)
lines(newdata.Topt$Topt,predict(fung.lm4,newdata=newdata.Topt), col=4)

fung.gam = gam( T.14 ~ s(Topt))
summary(fung.gam)

plot(fung.gam)

plot(Topt,T.14)
lines(newdata.Topt$Topt,predict(fung.gam,newdata=newdata.Topt)) 

fung.loess2 = loess (T.14 ~ Topt+Wmin)
summary(fung.loess2)

newdata.ToptWmin = expand.grid(Topt=seq(8,10,len=101),Wmin=seq(4,5,len=11))

predict.loess2 = predict(fung.loess2, newdata=newdata.ToptWmin)
perspPlus(seq(8,10,len=101), seq(4,5,len=11), predict.loess2, 
       nomx="Topt",nomy="Wmin",nomz="Durée d'humectation")

## 8-10 et 4-5 sont les intervalles de variation des facteurs Topt et Wmin

perspPlus(seq(8,10,len=101), seq(4,5,len=11), predict.loess2, 
     nomx="Topt",nomy="Wmin",nomz="Durée d'humectation",type=4)
points3d(Topt,Wmin,T.14, col=3)
spheres3d(Topt, Wmin, T.14,col="grey", radius = 0.03)

for(i in 1:4) {
# dev.new()
fung.multlm = lm (T.14 ~ polym(Topt,Wmin,degree=i))

predict.multlm = predict(fung.multlm, newdata=newdata.ToptWmin)

perspPlus(seq(8,10,len=101),seq(4,5,len=11), matrix(predict.multlm,ncol=11),
  nomx="Topt",nomy="Wmin",nomz="Durée d'humectation")
title(main= paste("degré",i))
}
summary(fung.multlm)

# dev.new()
perspPlus(Topt,Wmin,T.14)

# graphics.off() pour détruire toutes les fenêtres graphiques

# il faut toujours faire attention au format de sortie des fonctions (dim(predict.multlm))

fung.gam2 = gam( T.14 ~ s(Topt,Wmin))
plot(fung.gam2)
summary(fung.gam2)

predict.gam2 = predict(fung.gam2, newdata=newdata.ToptWmin)
perspPlus(seq(8,10,len=101),seq(4,5,len=11),matrix(predict.gam2,ncol=11), 
  nomx="Topt",nomy="Wmin",nomz="Durée d'humectation")

ish.plan = lhs.plan(50, plage=ishigami.factors)
sortie.ish = ishigami.simule(ish.plan, tout=TRUE)
ish.gam2 = gam(res~ s(x1,x2),data=sortie.ish)
plot(ish.gam2)

fung.gam3 = gam( T.14 ~ s(Topt)+s(Wmin))

cat("Ne pas oublier de faire RETURN dans la fenêtre de commande sous Linux\n")

plot(fung.gam3, pages=1)
summary(fung.gam3)

predict.gam3 = predict(fung.gam3, newdata=newdata.ToptWmin)

perspPlus(seq(8,10,len=101),seq(4,5,len=11),matrix(predict.gam3,ncol=11), nomx="Topt",nomy="Wmin",nomz="Durée d'humectation")

perspPlus(seq(8,10,len=101),seq(4,5,len=11),matrix(predict.gam2,ncol=11), 
  nomx="Topt",nomy="Wmin",nomz="Durée d'humectation")

detach(sortie.fung)


set.seed(2010)
fung.plan2 = lhs.plan(1000, plage=fungus.factors)
sortie2.fung = fungus.simule(fung.plan2, temperature = c(10,14, 18), tout=TRUE)
attach(sortie2.fung)

fung.plmm3 = plmm.mtk(sortie2.fung[,c("Tmin","Topt","Tmax", "Wmin", "Wmax", "T.14")],
  degpol = 3)
summary(fung.plmm3)
summary.plmm(fung.plmm3)
plot(fung.plmm3)
plot.plmm(fung.plmm3)

fung.iden = lm ( T.14 ~ polym(Tmin,Topt,Tmax, Wmin, Wmax, degree=3))
options(width=120) # si on veut des lignes plus longues
summary(fung.iden)


fung.iden2 = lm ( T.14 ~ polym(Tmin,Topt,Tmax, Wmin, degree=3)) # on a enlevé Wmax
summary(fung.iden2)
anova(fung.iden2,fung.iden) # compare deux modélisations (on retrouve l'effet total de Wmax)

fung.iden3 = lm ( T.14 ~ polym(Tmin,Topt,Tmax, degree=3)*Wmin)
summary(fung.iden3) 
anova(fung.iden2,fung.iden3)

fung.iden4 = lm ( T.14 ~ polym(Tmin,Topt,Tmax, degree=3):Wmin)
summary(fung.iden4) 

fung.iden4raw = lm ( T.14 ~ polym(Tmin,Topt,Tmax, degree=3, raw=TRUE):Wmin)
summary(fung.iden4raw) 

fung.iden4rawm = lm ( T.14 ~ -1 + polym(Tmin,Topt,Tmax, degree=3, raw=TRUE):Wmin)
summary(fung.iden4rawm)

summary(lm ( T.14 ~ -1 + polym(Tmin,Topt,Tmax, degree=2, raw=TRUE):Wmin))

summary(lm ( T.14/Wmin ~ polym(Tmin,Topt,Tmax,Wmax, degree=3)))

