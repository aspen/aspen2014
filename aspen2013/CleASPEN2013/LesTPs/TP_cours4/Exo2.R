rm(list=ls(all.names=TRUE))
library(lhs)
library(DiceKriging)
library(DiceView)

######################## BASE D'APPRENTISSAGE et BASE de TEST ##########################
N_BA = 15                ## Nombre de points de la base d'apprentissage (BA)
d = 1                     ## Dimension de l'espace des entrees
S_BA = .....              ## Simulation de la BA par plan LHS optimis� => utilisation de la fx optimumLHS
N_BT = 100                 ### Nombre de points de la base de test
S_BT = seq(0,1,length.out=N_BT)    ### Simulation d'une premiere base de test
                           ## S_BT : entrees de la base de test
                           ## Y_BT : sortie du simulateur

fx_exo2 <- function(X)
  sin(30*(X-0.9)^4)*cos(2*(X-0.9)) + (X-0.9)/2

Y_BA <- fx_exo2(S_BA)   ## Y_BA : sortie du simulateur sur la BA
Y_BT <- fx_exo2(S_BT)   ## Y_BT : sortie du simulateur sur la BT

plot(S_BT,Y_BT,type='l')
points(S_BA,Y_BA,pch=3,col=2)
legend("topright",y=c(0.17,0.33),c('Fonction th�orique','Pts de la base d apprentissage'),lty=c(1,0),
pch=c(-1,3),col=c(1,2),cex=0.8)

metamodel <- ....  # Creation du metamodele processus gaussien => utilisation de la fx km
plot(metamodel)   # Affichage des resultats

prediction <- ....  # Pr�diction du metamodele sur la base de test => utilisation de la fx predict.km

par(mfrow=c(2,1))
plot(S_BT,Y_BT,type='l',main='Estimation de f par m�tamod�le PG')
points(S_BA,Y_BA,pch=3,col='red')
lines(S_BT,prediction$mean,col='blue')
lines(S_BT,prediction$lower95,col='blue',lty=2)
lines(S_BT,prediction$upper95,col='blue',lty=2)
plot(S_BT,prediction$sd,type='l',main="sqrt(MSE) de l'estimation")

# avec DiceView
par(mfrow=c(2,1))
sectionview(metamodel)
plot(S_BT,prediction$sd,type='l',main="sqrt(MSE) de l'estimation")

#### Calcul du Q2 su BT ####
# Creation de la fonction qui permet de calculer le Q2
Q2 <- function(y,yhat)
  1-mean((y-yhat)^2)/var(y)

Q2_BT <- ... # Calcul du Q2 sur la Base de Test => utilisation de la fonctionQ2 d�finie ci-dessus

#### Affichage des resultats ####
print(paste('Nombre de points de la base d apprentissage :',N_BA))
print(paste('Dimension de l espace des entree :',d))
print(paste('Q2 sur Base de test :',Q2_BT))

#### Partie optionnelle ####
## Etape 3 : Calcul de la vraisemblance et optimisation des hyperparam�tres
x <- seq(0.01,1,length=1000)
y <- sapply(x,logLikFun,model=metamodel)
plot(x,y,type='l',main='Log-vraissemblance en fonction de theta')

theta_optim <- .... # Identification du theta optimal => utilisation de la fx which.max

## Etape 4 : planification adaptative
par(mfrow=c(2,1))
sectionview(metamodel,xlim=c(0,1))
plot(S_BT,prediction$sd,type='l',main="sqrt(MSE) de l'estimation")

## Planification adaptative : une it�ration
maxi <- which.max(prediction$sd)  # Recherche du point o� le MSE est maximal
points(S_BT[maxi],prediction$sd[maxi],pch=16,col=3)
S_BA2 <- rbind(S_BA,S_BT[maxi])   # rajout du point dans la BA
Y_BA2 <- c(Y_BA,fx_exo2(S_BT[maxi])) # rajout du Y correspondant dans la BA
metamodel2 <- km(formula=~1,design=S_BA2,response=Y_BA2,covtype="matern3_2") # Mise � jour du m�tamod�le PG
prediction2 <- predict.km(metamodel2,S_BT,'UK',checkNames=FALSE) # Mise � jour des pr�dictions et du MSE du m�tamod�le

# Affichage
par(mfrow=c(2,1))
sectionview(metamodel2)
plot(S_BT,prediction2$sd,type='l',main="sqrt(MSE) de l'estimation avec ajout d'un point")
points(S_BT[maxi],prediction2$sd[maxi],pch=16,col=3)

## Planification adaptative : Automatisation du processus
nIte <- 15
for (i in 1:nIte){ 
  maxi <- which.max(prediction2$sd)   # Recherche du point o� le MSE est maximal
	S_BA2 <- rbind(S_BA2,S_BT[maxi])    # rajout du point dans la BA
	Y_BA2 <- c(Y_BA2,fx_exo2(S_BT[maxi]))
	capture.output(metamodel2 <- km(formula=~1,design=S_BA2,response=Y_BA2,covtype="matern3_2")) # Mise � jour du m�tamod�le PG
	prediction2 <- predict.km(metamodel2,S_BT,'UK',checkNames=FALSE) # Mise � jour des pr�dictions et du MSE du m�tamod�le
  
  # Affichage
  par(mfrow=c(2,1))
  sectionview(metamodel2)
  plot(S_BT,prediction2$sd,type='l',main="sqrt(MSE) de l'estimation avec ajout d'un point")
  points(S_BT[maxi],prediction2$sd[maxi],pch=16,col=3)
  Sys.sleep(2)
}
# Rque : capture.output empeche km d'afficher � chaque fois les r�sultats de l'optim





