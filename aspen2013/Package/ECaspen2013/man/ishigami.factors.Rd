\name{ishigami.factors}
\alias{ishigami.factors}
\title{Facteurs d'entree du modele Ishigami}
\description{Facteurs d'entrée du modèle Ishigami, décrit dans \cite{Saltelli et al., 2000}}
\docType{data}
\value{data.frame à 3 lignes (facteurs) et 4 colonnes (spécifs)}


