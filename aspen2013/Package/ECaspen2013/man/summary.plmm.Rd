\name{summary.plmm}
\alias{summary.plmm}
\title{
indices de sensibilité issus d'un métamodèle polynomial
}
\description{
Imprime les indices de sensibilité issus d'un métamodèle polynomial
}
\usage{
\method{summary}{plmm}(object, lang = "en", ...)
}
\arguments{
  \item{object}{
résultat de la fonction \code{\link{plmm.mtk}}
}
  \item{lang}{
langue utilisée dans le graphique
}
  \item{...}{
autres paramètres
}
}
\author{
RF
}
