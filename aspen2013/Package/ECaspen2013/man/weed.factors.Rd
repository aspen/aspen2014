\name{weed.factors}
\alias{weed.factors}
\title{Facteurs d'entree du modele "Weed"}
\description{Facteurs d'entrée du modèle "Weed" (ou "mauvaises herbes")}
\docType{data}
\value{data.frame à 20 lignes (facteurs) et 4 colonnes (spécifs)}
