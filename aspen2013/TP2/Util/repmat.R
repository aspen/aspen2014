repmat <- function(X,nr,nc)
{
  if (! is.numeric(X)) stop(" X doit etre une numeric")
  
  if (is.matrix(X)) {
    rx <- dim(X)[1]
    cx <- dim(X)[2]
    return(matrix(t(matrix(X,rx,cx*nc)),rx*nr,cx*nc,byrow=T))
  }
  else stop("Erreur dans repmat: X doit de classe matrix")
}
