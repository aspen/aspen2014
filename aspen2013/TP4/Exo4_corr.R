rm(list=ls(all.names=TRUE))
library(lhs)
library(DiceKriging)
library(rgl)
library(sensitivity)


#### BASE D'APPRENTISSAGE et BASE de TEST ####

N_BA <- 80;                 ## Nombre de points de la base d'apprentissage
d <- 3;                     ## Dimension de l'espace des entrees
S_BA <- randomLHS(N_BA,d)   ## Simulation de la base d'apprentissage
                            ## S_BA : entrees
                            ## Y_BA : sortie du simulateur
S_BT <- randomLHS(100,3)    ## Simulation de la base de test

ishigami <- function(X,coeff){
  ## Analytical Function of Ishigami for scaled inputs => inputs in [0:1]
	## Ishigami function : Y = sinX1+a*(sinX2).^2 + b*X3.^4*sinX1 for Xi ~ U[-pi;+pi]
	## with a = coeff(1); b=coeff(2) 
	## Generally : coeff = [7 0.1];
	X <- 2*pi*X-pi
	sin(X[,1]) + coeff[1]*sin(X[,2])^2 + coeff[2]*X[,3]^4*sin(X[,1])
}

coeff_fx_test = c(7, 0.1) 
Y_BA <- ishigami(S_BA,coeff_fx_test)
Y_BT <- ishigami(S_BT,coeff_fx_test)

#### METAMODELING ####
metamodel <- km(formula=~1,design=S_BA,response=Y_BA,covtype="matern5_2")
plot(metamodel) # Affichage des resultats

## Calcul du Q2 sur une base de test
Q2 <- function(y,yhat)
  1-mean((y-yhat)^2)/var(y)

prediction <- predict(metamodel, S_BT, 'UK', checkNames=FALSE)$mean
Q2_BT<-Q2(Y_BT,prediction)
print(paste('Q2 sur Base de test :',Q2_BT))

## Calcul du Q2 par validation crois�e
prediction_LOO <- leaveOneOut.km(metamodel,'UK')$mean
Q2_LOO<- Q2(Y_BA,prediction_LOO)
print(paste('Q2 sur Base de test :',Q2_LOO))


#### Estimation des indices des cartes d'indices de Sobol du 1er ordre et totaux ####
# Creation de la fonction qui calcule les predictions du m�tamod�le, 
# pour laquelle on calcule les indices de Sobol
f <- function(x) 
  predict(metamodel,x,type='UK',checkNames=FALSE)$mean

## Calcul des indices par la m�thode Sobol et al. [2007]
n <- 10000
sample1 <- data.frame(X1=runif(n,0,1),X2=runif(n,0,1),X3=runif(n,0,1))
sample2 <- data.frame(X1=runif(n,0,1),X2=runif(n,0,1),X3=runif(n,0,1))
indices.sobol <- sobol2007(f,sample1,sample2) # utilisation de la fonction sobol2007

## Calcul des indices par la m�thode Fast (Saltelli et al. [1999])
n_RBDFast <- 5000
M_RBDFast <- 4
indices.fast <- fast99(model=f,factors=d,n=n_RBDFast,M=M_RBDFast,q='qunif',q.arg=list(min=0,max=1))

plot(indices.sobol)
plot(indices.fast)

#### Comparaison avec indices th�oriques ####
ishigami.sobol.indices <- function(coeff){
	D <- (coeff[1]^2)/8 + coeff[2]*(pi^4)/5 + coeff[2]^2*(pi^8)/18 + 0.5
	D1 = coeff[2]*(pi^4)/5 + coeff[2]^2*(pi^8)/50 + 0.5
	D2 = (coeff[1]^2)/8
	D13 = coeff[2]^2*(pi^8)*(1/18-1/50)
	S1 <- St <- array(0,3)
	S1[1] = D1/D
	S1[2] = D2/D
	S1[3] = 0
	St[1] = (D1+D13)/D
	St[2] = D2/D
	St[3] = D13/D
	list(S1=S1,St=St)
}
# Calcul des indices th�oriques
  indices.theo <- ishigami.sobol.indices(coeff_fx_test)

# Comparaison des indices estim�s et th�oriques
print('Indices th�oriques :')
indices.theo
print('Indices estim�s par Sobol2007 :')
indices.sobol
print('Indices estim�s par fast1999 :')
indices.fast









