\name{TP1indices.aov}
\alias{TP1indices.aov}
\title{Calcul et representation graphique des indices principaux et totaux}
\usage{TP1indices.aov(table.aov, noms, modeleAOV, titre="")
}
\description{Calcul et représentation graphique des indices principaux et totaux
issus d'une anova}
\arguments{\item{table.aov}{table d'anova issue de la fonction aov()}
\item{noms}{vecteur des labels des facteurs}
\item{modeleAOV}{modèle d'anova créé avec formula()}
\item{titre}{titre du graphique}
}

