lhs.plan <- function (taille, plage, repet = NULL, tout = FALSE, improved = TRUE, dup=1) 
{
## Improved Latin Hypercube Sample du package lhs
##      dup: A factor that determines the number of candidate points used in the search. 
##      A multiple of the number of remaining points than can be added.
    if (improved) plan = improvedLHS(taille, nrow(plage), dup=dup) else plan = randomLHS(taille, nrow(plage))
    tirage.lhs = as.data.frame(plan)
    names(tirage.lhs) = plage$name
    plan = lhs2intervalle(plan, plage[, c("binf", "bsup")])
    plan = as.data.frame(plan)
    names(plan) = plage$name
    if (!is.null(repet)) {
        rep = sample(seq(repet[, "binf"], repet[, "bsup"]), taille, 
            replace = TRUE)
        plan = cbind(plan, rep)
        names(plan) = c(plage$name, repet[, "name"])
    }
    if (tout) 
        retour = list(plan = plan, tirage.lhs = tirage.lhs)
    else retour = plan
    return(retour)
}
###############################################################################
quadrillage <- function(taille, plage=data.frame(binf=c(0,0), bsup = c(0,0) ) )
{
suite <- matrix(c(plage$binf,c(plage$bsup[1],plage$binf[2]),plage$bsup,c(plage$binf[1],plage$bsup[2]), plage$binf),
 ncol=2,byrow=T)
lines(suite[,1],suite[,2],col=2,lwd=2)
abline(v=seq(plage[1,"binf"], plage[1,"bsup"],length=taille+1), lty=2)
abline(h=seq(plage[2,"binf"], plage[2,"bsup"],length=taille+1), lty=2)
invisible()
}
###############################################################################
wwdm.simule <- function (X, year = NULL, jours.out = NULL, tout = FALSE, transfo = FALSE, 
    b1 = wwdm.factors$binf[1:Nbfac], b2 = wwdm.factors$bsup[1:Nbfac]) 
{
    if (transfo) {
        Nbfac <- ncol(X)
        X <- t(b1 + t(X) * (b2 - b1))
    }

	noms.out = "Biomasse"
	if(!is.null(jours.out)) noms.out = c(noms.out,paste("MS",jours.out,sep="."))

    if (is.null(year)) 
        sortie <- apply(X, 1, function(v, jours) { 
				traj <- wwdm.model(v[1:7], v[8])
				sort0 <- sum(traj)
				if(!is.null(jours)) sort0 <- c(sort0,traj[jours])
				sort0
				}, jours = jours.out)
    else 
	sortie <- apply(X, 1, function(v, jours) { 
				traj <- wwdm.model(v[1:7], year)
				sort0 <- sum(traj)
				if(!is.null(jours)) sort0 <- c(sort0,traj[jours])
				sort0
				}, jours = jours.out)


	if(is.null(jours.out))
			sortie <- data.frame(Biomasse = sortie)
		else
			sortie <- as.data.frame(t(sortie))
	names(sortie) <- noms.out

    if (tout) 
        sortie = cbind(X, sortie)
    return(sortie)
}
###############################################################################
###############################################################################
wwdm.simuleBase <- function (X, year = NULL, jours.out = NULL, tout = FALSE, transfo = FALSE, 
    b1 = wwdm.factors$binf[1:Nbfac], b2 = wwdm.factors$bsup[1:Nbfac]) 
{
    if (transfo) {
        Nbfac <- ncol(X)
        X <- t(b1 + t(X) * (b2 - b1))
    }

    if (is.null(year)) 
        trajectory <- apply(X, 1, function(v) wwdm.model(v[1:7], v[8]) )
    else 
	trajectory <- apply(X, 1, function(v) wwdm.model(v[1:7], year) )

	sortie = data.frame(Biomasse = apply(trajectory,2,sum))
	noms.out = "Biomasse"

	if(!is.null(jours.out)) {
		noms.out = c(noms.out,paste("MS",jours.out,sep="."))
		sortie <- cbind(sortie, t(trajectory)[ ,jours.out])
	}
#	dimnames(sortie) <- list(NULL, noms.out)
	names(sortie) <- noms.out

    if (tout) 
        sortie = cbind(X, sortie)
    return(sortie)
}
###############################################################################

